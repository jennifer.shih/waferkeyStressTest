/* eslint-disable no-undef */
import http from "k6/http";
import { htmlReport } from "https://raw.githubusercontent.com/benc-uk/k6-reporter/main/dist/bundle.js";

export const options = {
  vus: 1,
  iterations: 1,
};

const baseUrl = "https://waferkeyapi-dev.waferlock.com/api/v2";
const loginUrl = `${baseUrl}/Member/Login`;
const firebaseloginUrl = `http://172.16.12.50:8081/firebase/login`;
const email = "jennifer.shih@waferlock.com";

//Firebase Login
export function firebaseLogin(email) {
  const firebaseLoginPayload = {
    email: email,
    pw: __ENV.FIXED_PASSWORD,
  };

  const headers = { "Content-Type": "application/json" };
  const response = http.post(firebaseloginUrl, JSON.stringify(firebaseLoginPayload), {
    headers: headers,
  });

  if (response.status === 200) {
    const responseData = JSON.parse(response.body);
    const idToken = responseData.idToken;
    if (idToken) {
      console.log("Firebase Login Successful");
      return idToken;
    } else {
      console.error("Firebase Login Failed:", response.status, response.body);
    }
  }
}

//api登入
export function login(idToken) {
  const loginPayload = { IdToken: idToken };

  const headers = { "Content-Type": "application/json" };
  const response = http.post(loginUrl, JSON.stringify(loginPayload), {
    headers: headers,
  });

  if (response.status === 200) {
    console.log("Login Successful");
    return idToken;
  } else {
    console.error("Login Failed:", response.status, response.body);
  }
}

export default function () {
  const idToken = firebaseLogin(email);
  if (idToken) {
    login(idToken);
  }
}

//生成報告
export function handleSummary(data) {
  const report = htmlReport(data);
  const currentDate = new Date().toISOString().slice(0, 10);

  return { [`account/login_report_${currentDate}.html`]: report };
}

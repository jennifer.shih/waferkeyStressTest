/* eslint-disable no-undef */
import http from "k6/http";
import { sleep } from "k6";
import { htmlReport } from "https://raw.githubusercontent.com/benc-uk/k6-reporter/main/dist/bundle.js";

export const options = {
  vus: 1,
  iterations: 1,
};

const fixedPassword = __ENV.FIXED_PASSWORD;
const baseUrl = "https://waferkeyapi-dev.waferlock.com/api/v2";
const loginUrl = `${baseUrl}/Auth/login`;
const membersUrl = `${baseUrl}/Member`;
export let authToken = "";

//取得AuthToken
export function getAuthToken() {
  const loginPayload = {
    username: __ENV.USERNAME,
    password: __ENV.PASSWORD,
  };
  const loginHeaders = {
    "Content-Type": "application/json",
  };

  const response = http.post(loginUrl, JSON.stringify(loginPayload), {
    headers: loginHeaders,
  });

  if (response.status === 200) {
    const responseData = JSON.parse(response.body);
    authToken = responseData.data.accessToken;
    console.log("Get Auth Token Successful");
    return authToken;
  } else {
    console.error("Get Auth Token Failed", response.status, response.body);
  }
}

//取得已建立的帳號
export function getExistingAccounts() {
  const headers = {
    Authorization: `Bearer ${authToken}`,
    "Content-Type": "application/json",
  };

  const response = http.get(membersUrl, { headers: headers });

  if (response.status === 200) {
    const responseData = JSON.parse(response.body);
    return responseData.data;
  } else {
    console.error("Get Existing Accounts Failed", response.status, response.body);
  }
}

//建立帳號
export function addAccount(username) {
  const email = `${username}@waferlock.com`;

  const user = {
    uid: null,
    displayName: username,
    email: email,
    phoneNumber: null,
    photoUrl: null,
    emailVerified: true,
    disabled: false,
    Password: fixedPassword,
  };

  const headers = {
    Authorization: `Bearer ${authToken}`,
    "Content-Type": "application/json",
  };

  const res = http.post(membersUrl, JSON.stringify(user), {
    headers: headers,
  });

  if (res.status === 200) {
    console.log(`Registered User ${username} Succesfully`);
  } else {
    console.error(`Registered User ${username} Failed`);
  }

  sleep(2);
}

export default function () {
  getAuthToken();
  const existingAccounts = getExistingAccounts();

  let maxNumber = 0;
  for (const account of existingAccounts) {
    if (account.email) {
      const match = account.email.match(/jennifer\.shih\+(\d+)/);
      if (match && match[1]) {
        const userNumber = parseInt(match[1]);
        if (userNumber > maxNumber) {
          maxNumber = userNumber;
        }
      }
    }
  }

  const startNumber = maxNumber + 1;

  for (let i = 0; i < 2; i++) {
    const newAccountCounter = startNumber + i;
    const username = `jennifer.shih+${newAccountCounter}`;
    addAccount(username);
  }
}

//生成報告
export function handleSummary(data) {
  const report = htmlReport(data);
  const currentDate = new Date().toISOString().slice(0, 10);

  return { [`account/signup_report_${currentDate}.html`]: report };
}

import http from "k6/http";
import { login, firebaseLogin } from "./login.js";
import { getAuthToken, getExistingAccounts } from "./signup.js";
import { htmlReport } from "https://raw.githubusercontent.com/benc-uk/k6-reporter/main/dist/bundle.js";

export const options = {
  vus: 1,
  iterations: 1,
};

const baseUrl = "https://waferkeyapi-dev.waferlock.com/api/v2";
const deleteOwnerLockUrl = `${baseUrl}/Member/Self`;

//刪除帳號
export function deleteAccount(account) {
  const idToken = firebaseLogin(account.email);
  if (idToken) {
    const loggedInIdToken = login(idToken);
    const headers = {
      Authorization: `Bearer ${loggedInIdToken}`,
      "Content-Type": "application/json",
    };
    const response = http.del(deleteOwnerLockUrl, null, { headers: headers });

    if (response.status === 200) {
      console.log(`Deleted User ${account.displayName} Successfully`);
    } else {
      console.error(`Deleted User ${account.displayName} Failed`, response.status, response.body);
    }
  }
}

export default function () {
  getAuthToken();

  const existingAccounts = getExistingAccounts();
  for (const account of existingAccounts) {
    if (account.email && account.email.startsWith("jennifer.shih+")) {
      deleteAccount(account);
    }
  }
}

//生成報告
export function handleSummary(data) {
  const report = htmlReport(data);
  const currentDate = new Date().toISOString().slice(0, 10);

  return { [`account/delete_report_${currentDate}.html`]: report };
}

import http from "k6/http";
import { firebaseLogin, login } from "../account/login.js";
import { getAuthToken } from "../account/signup.js";
import { htmlReport } from "https://raw.githubusercontent.com/benc-uk/k6-reporter/main/dist/bundle.js";

export const options = {
  vus: 1,
  iterations: 1,
};

const baseUrl = "https://waferkeyapi-dev.waferlock.com/api/v2";
const devicename = "L376D1B55CF45552";
const ownerLockUrl = `${baseUrl}/Ownerlock/Lock/${devicename}`;
const email = "jennifer.shih@waferlock.com";

//取得鎖的擁有者資料
export function getExistingOwnerLock() {
  const idToken = firebaseLogin(email);

  if (idToken) {
    const loggedInIdToken = login(idToken);
    const headers = {
      Authorization: `Bearer ${loggedInIdToken}`,
      "Content-Type": "application/json",
    };

    const response = http.get(ownerLockUrl, { headers: headers });

    if (response.status === 200) {
      const responseData = JSON.parse(response.body);
      const ownerLocks = responseData.data.ownerlock;

      const ownerlockIds = ownerLocks.map((ownerlock) => ownerlock.id); // 提取所有ownerlock的id
      console.log("Get Existing OwnerLocks Successfully", ownerlockIds);
      return ownerlockIds;
    } else {
      console.error("Get Existing OwnerLocks Failed", response.status, response.body);
    }
  }
}

//編輯鎖的擁有者資料
export function updateOwnerLock(ownerlockId, editOwnerLockUrl) {
  const idToken = firebaseLogin(email);

  if (idToken) {
    const loggedInIdToken = login(idToken);

    const lock = {
      putOwnerlock: {
        displayname: "測試改名字",
      },
      openingMethod: {
        allowAccess: 1,
        canAppAccess: 1,
        canRemoteAccess: 0,
      },
    };

    const headers = {
      Authorization: `Bearer ${loggedInIdToken}`,
      "Content-Type": "application/json",
    };
    const response = http.put(editOwnerLockUrl, JSON.stringify(lock), { headers: headers });

    if (response.status === 200) {
      const responseData = JSON.parse(response.body);
      console.log("Update OwnerLockID Successfully", responseData.data.ownerlockId);
    } else {
      console.error("Update OwnerLockID Failed", response.status, response.body);
    }
  }
}

export default function () {
  getAuthToken();

  const ownerlockIds = getExistingOwnerLock();
  if (ownerlockIds && ownerlockIds.length > 0) {
    for (const ownerlockId of ownerlockIds) {
      const editOwnerLockUrl = `${baseUrl}/Ownerlock/All/${ownerlockId}`;
      updateOwnerLock(ownerlockId, editOwnerLockUrl);
    }
  }
}

//生成報告
export function handleSummary(data) {
  const report = htmlReport(data);
  const currentDate = new Date().toISOString().slice(0, 10);

  return { [`ownerlock/editOwnerLock_report_${currentDate}.html`]: report };
}

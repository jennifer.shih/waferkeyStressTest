import http from "k6/http";
import { firebaseLogin, login } from "../../account/login.js";
import { getAuthToken } from "../../account/signup.js";
import { htmlReport } from "https://raw.githubusercontent.com/benc-uk/k6-reporter/main/dist/bundle.js";

export const options = {
  vus: 1,
  iterations: 1,
};

const baseUrl = "https://waferkeyapi-dev.waferlock.com/api/v2";
const devicename = "L376D1B55CF45552";
const randomPasscodeUrl = `${baseUrl}/Lock/RandomPasscode/${devicename}`;
const email = "jennifer.shih@waferlock.com";

//取得鎖的隨機密碼
export function getRandomPasscode() {
  const idToken = firebaseLogin(email);

  if (idToken) {
    const loggedInIdToken = login(idToken);
    const headers = {
      Authorization: `Bearer ${loggedInIdToken}`,
      "Content-Type": "application/json",
    };

    const response = http.get(randomPasscodeUrl, { headers: headers });

    if (response.status === 200) {
      const responseData = JSON.parse(response.body);
      const randomPasscode = responseData.data;
      console.log(`Generate Random Passcode ${randomPasscode} Succesfully`);
      return randomPasscode;
    } else {
      console.error("Generate Random Passcode Failed", response.status, response.body);
    }
  }
}

export default function () {
  getAuthToken();
  getRandomPasscode();
}

//生成報告
export function handleSummary(data) {
  const report = htmlReport(data);
  const currentDate = new Date().toISOString().slice(0, 10);

  return { [`ownerlock/withPassCode/getRandomPasscode_report_${currentDate}.html`]: report };
}

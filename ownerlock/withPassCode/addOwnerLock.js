/* eslint-disable no-undef */
import http from "k6/http";
import { firebaseLogin, login } from "../../account/login.js";
import { getAuthToken, getExistingAccounts } from "../../account/signup.js";
import { getRandomPasscode } from "./getRandomPasscode.js";
import { htmlReport } from "https://raw.githubusercontent.com/benc-uk/k6-reporter/main/dist/bundle.js";

export const options = {
  vus: 1,
  iterations: 1,
};

const baseUrl = "https://waferkeyapi-dev.waferlock.com/api/v2";
const devicename = "L376D1B55CF45552";
const addOwnerLockUrl = `${baseUrl}/Ownerlock/All/Lock/${devicename}`;
const firebaseEmail = "jennifer.shih@waferlock.com";

//新增電子鎖
export function addOwnerLockWithPassCode(email, passcode, displayname) {
  const idToken = firebaseLogin(firebaseEmail);

  if (idToken) {
    const loggedInIdToken = login(idToken);

    const headers = {
      Authorization: `Bearer ${loggedInIdToken}`,
      "Content-Type": "application/json",
    };

    const type = __ENV.type || "Owner"; // 默認是Owner

    // Define different payloads based on scheduleType
    let payload = {};

    if (type === "Owner") {
      payload = {
        MemberUID: "",
        putOwnerlock: {
          role: 0,
          displayname: displayname,
        },
        openingMethod: {
          CanAppAccess: 1,
          CanRemoteAccess: 1,
          Passcode: passcode,
        },
        schedule: {
          scheduleType: 0,
          diff: 8,
        },
      };
    } else if (type === "Recurring") {
      payload = {
        MemberUID: "",
        putOwnerlock: {
          role: 1,
          displayname: displayname,
        },
        openingMethod: {
          CanAppAccess: 1,
          CanRemoteAccess: 1,
          Passcode: passcode,
        },
        schedule: {
          scheduleType: 1,
          diff: 8,
          scheduleType1Data: {
            scheduleRecurring: [
              {
                isSelectSunday: 0,
                isSelectMonday: 1,
                isSelectTuesday: 1,
                isSelectWednesday: 1,
                isSelectThursday: 0,
                isSelectFriday: 0,
                isSelectSaturday: 0,
                recurringStartTime: "08:00:00",
                recurringEndTime: "17:00:00",
              },
            ],
          },
        },
      };
    } else if (type === "Temp") {
      payload = {
        MemberUID: "",
        putOwnerlock: {
          role: 1,
          displayname: displayname,
        },
        openingMethod: {
          CanAppAccess: 1,
          CanRemoteAccess: 1,
          Passcode: passcode,
        },
        schedule: {
          scheduleType: 2,
          diff: 8,
          scheduleType2Data: {
            temporaryStartDate: "2023-08-16T08:00:00Z",
            temporaryEndDate: "2023-08-16T16:00:00Z",
            temporaryOneTime: 0,
          },
        },
      };
    }

    const response = http.post(addOwnerLockUrl, JSON.stringify(payload), { headers: headers });

    if (response.status === 200) {
      console.log(`Add Owner Lock ${displayname} Succesfully`);
      const responseData = JSON.parse(response.body);
      const sharecode = responseData.data.shareCode;
      return sharecode;
    } else {
      console.error(`Add Owner Lock ${displayname} Failed`, response.status, response.body);
    }
  }
}

export default function () {
  getAuthToken();
  const existingAccounts = getExistingAccounts();

  for (const account of existingAccounts) {
    if (account.email) {
      const match = account.email.match(/jennifer\.shih\+(\d+)/);
      if (match && match[1]) {
        const email = account.email;
        const passcode = getRandomPasscode();
        const displayname = `${email} passcode測試用`;
        addOwnerLockWithPassCode(email, passcode, displayname);
      }
    }
  }
}

//生成報告
export function handleSummary(data) {
  const report = htmlReport(data);
  const currentDate = new Date().toISOString().slice(0, 10);

  return { [`ownerlock/withPassCode/addOwnerLock_report_${currentDate}.html`]: report };
}

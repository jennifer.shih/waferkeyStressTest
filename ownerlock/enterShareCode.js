import http from "k6/http";
import { firebaseLogin, login } from "../account/login.js";
import { getAuthToken, getExistingAccounts } from "../account/signup.js";
import { addOwnerLock } from "./addOwnerLock.js";
import { htmlReport } from "https://raw.githubusercontent.com/benc-uk/k6-reporter/main/dist/bundle.js";

export const options = {
  vus: 1,
  iterations: 1,
};

const baseUrl = "https://waferkeyapi-dev.waferlock.com/api/v2";

//輸入shareCode
function enterShareCode(email, shareCode) {
  const enterShareCodeUrl = `${baseUrl}/ShareLock/Link/${shareCode}`;
  const idToken = firebaseLogin(email);

  if (idToken) {
    const loggedInIdToken = login(idToken);
    const headers = {
      Authorization: `Bearer ${loggedInIdToken}`,
      "Content-Type": "application/json",
    };

    const response = http.get(enterShareCodeUrl, { headers: headers });

    if (response.status === 200) {
      console.log(`Enter ShareCode Succesfully`);
    } else {
      console.error("Enter ShareCode Failed", response.status, response.body);
    }
  }
}

export default function () {
  getAuthToken();
  const existingAccounts = getExistingAccounts();

  for (const account of existingAccounts) {
    if (account.email) {
      const match = account.email.match(/jennifer\.shih\+(\d+)/);
      if (match) {
        const email = account.email;
        const displayname = `${email} 測試用`;
        const shareCode = addOwnerLock(displayname);
        enterShareCode(email, shareCode);
      }
    }
  }
}

//生成報告
export function handleSummary(data) {
  const report = htmlReport(data);
  const currentDate = new Date().toISOString().slice(0, 10);

  return { [`ownerlock/enterShareCode_report_${currentDate}.html`]: report };
}

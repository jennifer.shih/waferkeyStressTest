import http from "k6/http";
import { firebaseLogin, login } from "../account/login.js";
import { getAuthToken } from "../account/signup.js";
import { getExistingOwnerLock } from "./editOwnerLock.js";
import { htmlReport } from "https://raw.githubusercontent.com/benc-uk/k6-reporter/main/dist/bundle.js";

export const options = {
  vus: 1,
  iterations: 1,
};

const baseUrl = "https://waferkeyapi-dev.waferlock.com/api/v2";
const email = "jennifer.shih@waferlock.com";

//刪除鎖的擁有者資料
export function deleteOwnerLock(ownerlockId) {
  getAuthToken();
  const idToken = firebaseLogin(email);
  if (idToken) {
    const loggedInIdToken = login(idToken);
    const headers = {
      Authorization: `Bearer ${loggedInIdToken}`,
      "Content-Type": "application/json",
    };
    const deleteOwnerLockUrl = `${baseUrl}/Ownerlock/${ownerlockId}`;

    const response = http.del(deleteOwnerLockUrl, null, { headers: headers });

    if (response.status === 200) {
      console.log(`Delete OwnerLockID ${ownerlockId} Successfully`);
    } else {
      console.error(`Delete OwnerLockID ${ownerlockId} Failed.`, response.status, response.body);
    }
  }
}

export default function () {
  getAuthToken();

  const ownerlockIds = getExistingOwnerLock();
  if (ownerlockIds && ownerlockIds.length > 0) {
    for (let i = 1; i < ownerlockIds.length; i++) {
      const ownerlockId = ownerlockIds[i];
      deleteOwnerLock(ownerlockId);
    }
  }
}

//生成報告
export function handleSummary(data) {
  const report = htmlReport(data);
  const currentDate = new Date().toISOString().slice(0, 10);

  return { [`ownerlock/deleteOwnerLock_report_${currentDate}.html`]: report };
}

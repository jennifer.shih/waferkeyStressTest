import http from "k6/http";
import { sleep } from "k6";
import { htmlReport } from "https://raw.githubusercontent.com/benc-uk/k6-reporter/main/dist/bundle.js";

export const options = {
  vus: 10, //模擬使用者數量
  iterations: 15, //模擬執行次數
};

export default function () {
  http.get("https://test.k6.io");
  sleep(1);
}

//生成報告
export function handleSummary(data) {
  return {
    "example/example_report.html": htmlReport(data),
  };
}

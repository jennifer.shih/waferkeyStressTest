# WAFERKEY 壓力測試

## 檔案資料夾說明

### account

- 資料夾主要放帳號相關的檔案。

### ownerlock

- 資料夾主要放成員相關的檔案。

### ownerlock/withPassCode

- 資料夾主要放有設定密碼相關的檔案。
